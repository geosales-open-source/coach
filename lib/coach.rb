# frozen_string_literal: true

require_relative "coach/coach_config"
require_relative "coach/http_controller"
require_relative "coach/midplayer_api"
require_relative "coach/version"

module Coach
  class Error < StandardError; end
end
