# frozen_string_literal: true

require "net/http"
require "uri"
require_relative "midplayer_api"

module Coach
  class Config
    attr_reader :base_dir, :token, :midplayer_addr

    def midplayer_url
      if @midplayer_url.nil? && @should_try_url
        begin
          @midplayer_url = URI.parse @midplayer_addr
        rescue URI::InvalidURIError => e
          warn "URI invalida passada, verificar por favor #{e}"
        ensure
          @should_try_url = false
        end
      end
      @midplayer_url
    end

    def midplayer_api
      return @midplayer_api unless @midplayer_api.nil?

      base = midplayer_url
      @midplayer_api = MidplayerApi.new base unless base.nil?
      @midplayer_api
    end

    def check_midplayer_version(force_check: false)
      return @midplayer_version if !@midplayer_version.nil? && !force_check

      resp = begin
               midplayer_api.about
             rescue Errno::ECONNREFUSED => e
               warn "conexao recusada: #{e}"
             end
      if resp.nil? || !resp.is_a?(Net::HTTPSuccess)
        warn "servico do midplayer nao esta no ar"
        @midplayer_version = nil
        return
      end
      @midplayer_version = resp.body
    end

    def midplayer_version
      check_midplayer_version force_check: false
    end

    # multi-tenant eh feito da seguinte maneira:
    # - #{base_dir}/#{tenant}/#{tabela}.json
    # - #{base_dir}/#{tenant}-#{tabela}.json
    #
    # se nao for detectada a existencia dessa maneira de multi-tenant,
    # o coach assumira que eh single-tenant:
    # - #{base_dir}/#{tabela}.json
    #
    # se mesmo assim nao existir o arquivo, ele sera ignorado sem consequencias
    #
    # se estiver em ambiente multi-tenant, organizar desse jeito os arquivos
    # sob o risco de corromper a base
    def initialize(base_dir, token, midplayer_addr)
      @base_dir = base_dir.end_with?("/") ? base_dir : base_dir + "/"
      @token = token
      @midplayer_addr = midplayer_addr

      @should_try_url = true
    end

    def ok?
      return false unless File.directory? @base_dir
      return false unless midplayer_url&.scheme&.start_with?("http")

      check_midplayer_version
      true
    end
  end
end
