# frozen_string_literal: true

require "time"

require_relative "coach_config"
require_relative "import_status"
require_relative "polymorph/polymorpherism"

module Coach
  class Controller
    attr_reader :config

    def self.create_controller(base_dir:, token:, midplayer_addr: "http://localhost:8016/")
      config = Config.new(base_dir, token, midplayer_addr)
      raise "Problemas na configuracao" unless config.ok?

      Controller.new(config)
    end

    def initialize(config)
      @config = config
      @polymorphed = false
    end

    def midplayer_api
      config.midplayer_api
    end

    def import_list(pagesize: nil, page: nil)
      check_polymorph
      raise "Não foi possível determinar o comportamento. Mid-player tá ligado?" unless @polymorphed

      @behaviour.import_list pagesize: pagesize, page: page
    end

    def tabelas_import(id)
      check_polymorph
      raise "Não foi possível determinar o comportamento. Mid-player tá ligado?" unless @polymorphed

      @behaviour.tabelas_import id
    end

    def do_import(tenant:, json_str: nil, json_file: nil)
      check_polymorph
      raise "Não foi possível determinar o comportamento. Mid-player tá ligado?" unless @polymorphed

      @behaviour.do_import tenant: tenant, json_str: json_str, json_file: json_file
    end

    def checa_refazer_importacao
      tenant_reenvios = {}
      import_list.group_by(&:tenant).each_pair do |tenant, imports|
        reenvios = julga_importacoes tenant, imports
        tenant_reenvios[tenant] = reenvios unless reenvios.nil?
      end
      tenant_reenvios
    end

    def julga_importacoes(tenant, imports)
      return nil if abortar_precoce_julga_importacoes? imports

      tabs_sucesso = Set.new
      tabs_falha = Set.new
      queued_or_running = false
      imports.each do |import|
        status = import.tipo_status
        if [TiposStatus::QUEUED, TiposStatus::RUNNING].include? status
          queued_or_running = true
          break
        end
        julga_importacao import, status, tabs_sucesso, tabs_falha
      end
      return nil if queued_or_running

      reenvia_falhas tenant, tabs_falha
    end

    def abortar_precoce_julga_importacoes?(imports)
      imports.size.zero? || imports[0].hora_ultimo > (Time.now.utc - 2 * 60 * 60)
    end

    def julga_importacao(import, status, tabs_sucesso, tabs_falha)
      if status == TiposStatus::FINISHED
        import.tabelas.each do |tabela|
          tabs_sucesso << tabela unless tabs_falha.include? tabela
        end
      elsif status == TiposStatus::FAILED
        import.tabelas.each do |tabela|
          tabs_falha << tabela unless tabs_sucesso.include? tabela
        end
      end
    end

    def reenvia_falhas(tenant, tabs_falha)
      reenviados = []
      tabs_falha.each do |tabela|
        file_name = determina_filename(tenant, tabela)
        next if file_name.nil?

        json_file = File.open file_name
        begin
          do_import tenant: tenant, json_file: json_file
          reenviados << tabela
        ensure
          json_file.close
        end
      end
      reenviados
    end

    def determina_filename(tenant, tabela)
      file_name_slash = config.base_dir + "#{tenant}/#{tabela}.json"
      return file_name_slash if File.exist? file_name_slash

      file_name_dash = config.base_dir + "#{tenant}-#{tabela}.json"
      return file_name_dash if File.exist? file_name_dash

      file_name_singletenant = config.base_dir + "#{tabela}.json"
      return file_name_singletenant if File.exist? file_name_singletenant

      nil
    end

    private

    def check_polymorph
      return if @polymorphed

      @behaviour = Polymorph.determinar_comportamento self
      @polymorphed = !@behaviour.nil?
    end
  end
end
