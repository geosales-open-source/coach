# frozen_string_literal: true

require "time"
require "typesafe_enum"

module Coach
  class TiposStatus < TypesafeEnum::Base
    new :QUEUED
    new :RUNNING
    new :FINISHED
    new :FAILED
    new :CANCELED
  end

  def self.to_time(hora)
    hora.is_a?(Time) ? hora : Time.parse(hora)
  end

  class ImportStatus
    attr_reader :id, :tenant, :status, :hora_inicio, :hora_ultimo

    # rubocop:disable Metrics/ParameterLists
    def initialize(id, tenant, status, hora_inicio, hora_ultimo, tabelas: nil, http_controller: nil)
      # rubocop:enable Metrics/ParameterLists
      @id = id
      @tenant = tenant
      @status = status
      @tabelas = tabelas
      @http_controller = http_controller
      @hora_inicio = Coach.to_time(hora_inicio)
      @hora_ultimo = Coach.to_time(hora_ultimo)
    end

    def tabelas
      return @tabelas unless @tabelas.nil?

      @tabelas = !@http_controller.nil? ? @http_controller.tabelas_import(@id) : []
    end

    def tipo_status
      Coach.tipo_status @status
    end
  end

  def self.tipo_status(status_string)
    return TiposStatus::QUEUED if status_string == "QUEUED"
    return TiposStatus::FAILED if status_string.start_with? "FAIL"
    return TiposStatus::FINISHED if status_string.start_with? "FINISHED"
    return TiposStatus::CANCELED if status_string.start_with? "CANCELED"

    TiposStatus::RUNNING
  end
end
