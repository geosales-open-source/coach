# frozen_string_literal: true

require "net/http"
require "uri"

module Coach
  class MidplayerApi
    AUTH_HEADER = "SSAUTH_TOKEN"

    def initialize(midplayer_uri)
      @midplayer_uri = midplayer_uri.is_a?(URI::HTTP) ? midplayer_uri : URI.parse(midplayer_uri)
      @midplayer_uri.path += "/" unless @midplayer_uri.path.end_with? "/"
    end

    def about
      Net::HTTP.get_response @midplayer_uri.merge "api/about"
    end

    def importlist_html(token:, pagesize: nil, page: nil)
      importstatus_url = @midplayer_uri.merge "importstatus"

      query = { "token": token }
      query[:pagesize] = pagesize unless pagesize.nil?
      query[:page] = page unless page.nil?
      importstatus_url.query = URI.encode_www_form query

      Net::HTTP.get_response importstatus_url
    end

    def importstatus(token:, id:)
      importstatus_url = @midplayer_uri.merge "api/importstatus/#{id}"
      http_req = Net::HTTP::Get.new importstatus_url
      http_req.add_field AUTH_HEADER, token
      Net::HTTP.start importstatus_url.hostname, importstatus_url.port do |http|
        http.request http_req
      end
    end

    def importstatus_html(token:, id:)
      importstatus_url = @midplayer_uri.merge "importstatus/#{id}"

      query = { "token": token }
      importstatus_url.query = URI.encode_www_form query

      Net::HTTP.get_response importstatus_url
    end

    def post_import_string(token:, tenant:, json_str: nil, json_file: nil)
      raise "Passar a string json e o arquivo json é inválido" unless json_str.nil? || json_file.nil?
      raise "Precisa passar o json, seja via string ou via arquivo" if json_str.nil? && json_file.nil?

      post_import_string_url = @midplayer_uri.merge "api/import/#{tenant}"
      http_req = Net::HTTP::Post.new post_import_string_url
      http_req.add_field AUTH_HEADER, token
      http_req.add_field "Content-type", "application/json"
      prepara_post_envio http_req, json_str, json_file
      Net::HTTP.start post_import_string_url.hostname, post_import_string_url.port do |http|
        http.request http_req
      end
    end

    private

    def prepara_post_envio(http_req, json_str, json_file)
      # garantido que json_str e json_file tera um nulo e um nao nulo
      if !json_str.nil?
        http_req.body = json_str unless json_str.nil?
      else
        http_req.body_stream = json_file
        http_req.add_field "Transfer-encoding", "chunked"
      end
    end
  end
end
