# frozen_string_literal: true

require "nokogiri"

module Coach
  module Polymorph
    def self.determinar_comportamento(http_controller)
      midplayer_version = version_as_string_list http_controller.config.midplayer_version
      if midplayer_version.size == 3
        if midplayer_version[0].zero? && midplayer_version[1].zero?
          if midplayer_version[2] <= 6
            obj_v_0_0_6 http_controller
          elsif midplayer_version[2] <= 7
            obj_v_0_0_7 http_controller
          end
        end
      end
      # so cai nesse caso na grande excessao
      # o obj_default nesse caso aqui eh o mais atualizado
      obj_default http_controller
    end

    # manter sempre com a versao mais atualizada
    def self.obj_default(http_controller)
      obj_v_0_0_7 http_controller
    end

    def self.obj_v_0_0_6(http_controller)
      comportamento = Object.new
      comportamento.define_singleton_method(:http_controller) { http_controller }
      comportamento.extend BehaviourV006

      comportamento
    end

    def self.obj_v_0_0_7(http_controller)
      comportamento = Object.new
      comportamento.define_singleton_method(:http_controller) { http_controller }
      comportamento.extend BehaviourV007

      comportamento
    end

    module BehaviourV007
      def import_list(pagesize:, page:)
        res = http_controller.midplayer_api.importlist_html pagesize: pagesize,
                                                            page: page, token: http_controller.config.token
        return [] unless res.is_a? Net::HTTPSuccess

        Polymorph.extrair_linhas_importacao_importstatus_html(res.body).map do |linha|
          Polymorph.linhas_table_import_status linha, http_controller
        end
      end

      def tabelas_import(id)
        res = http_controller.midplayer_api.importstatus_html token: http_controller.config.token, id: id
        return [] unless res.is_a? Net::HTTPSuccess

        tabelas_nodo_li = Polymorph.extrair_tabelas_nodo_li_tabelas_import_html res.body
        return [] if tabelas_nodo_li.empty?

        tabelas_nodo_li[0].children[1].text[2..].split(";")
      end

      def do_import(tenant:, json_str: nil, json_file: nil)
        http_controller.midplayer_api.post_import_string token: http_controller.config.token, tenant: tenant,
                                                         json_str: json_str, json_file: json_file
      end
    end

    module BehaviourV006
      include BehaviourV007

      def import_list(pagesize:, page:)
        super(pagesize: pagesize, page: page).reverse
      end
    end

    def self.version_as_string_list(version)
      version[1..].sub(/\+.*$/, "").split(".").map(&:to_i)
    end

    def self.extrair_tabelas_nodo_li_tabelas_import_html(html_body)
      doc = Nokogiri::HTML html_body

      info_extra = doc.css(".info-extra").select { |div| div.xpath("strong").text.start_with? "Informa" }
      return [] if info_extra.empty?

      info_extra[0].xpath("ul").css("li").select { |nodo| nodo.css("strong").text == "tabelas" }
    end

    def self.extrair_linhas_importacao_importstatus_html(html_body)
      doc = Nokogiri::HTML html_body
      table = doc.css "table"
      table.css("tr").reject { |table_line| table_line.parent.name == "thead" }
    end

    def self.linhas_table_import_status(linha, http_controller)
      colunas = linha.css("td")
      ImportStatus.new colunas[0].children[0].text, colunas[1].text, colunas[2].text,
                       colunas[3].text + " UTC", colunas[4].text + " UTC", http_controller: http_controller
    end
  end
end
