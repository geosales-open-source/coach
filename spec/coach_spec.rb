# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
RSpec.describe Coach do
  # rubocop:enable Metrics/BlockLength
  it "has a version number" do
    expect(Coach::VERSION).not_to be nil
  end

  it "creates a config with the detected version number as nil" do
    config = Coach::Config.new "./", "123", "http://localhost:8016"

    expect(config.midplayer_version).to be nil
  end

  it "verifica se identifica corretamente o tipo da enumeração baseado em seu nome" do
    workflow_basico = {
      "CANCELED" => Coach::TiposStatus::CANCELED,
      "CANCELED_USUARIO" => Coach::TiposStatus::CANCELED,
      "QUEUED" => Coach::TiposStatus::QUEUED,
      "PREPARAR_ENVIAR" => Coach::TiposStatus::RUNNING,
      "ENVELHECER_OK" => Coach::TiposStatus::RUNNING,
      "NAO_HA_NOVIDADES" => Coach::TiposStatus::RUNNING,
      "LIMPEZA_FALHA" => Coach::TiposStatus::RUNNING,
      "LIMPEZA_FALHA_ENVIO" => Coach::TiposStatus::RUNNING,
      "FAILED" => Coach::TiposStatus::FAILED,
      "FAILED_ENVIO" => Coach::TiposStatus::FAILED,
      "FINISHED" => Coach::TiposStatus::FINISHED,
      "FINISHED_NOT_ENVIO" => Coach::TiposStatus::FINISHED
    }

    workflow_basico.each do |nome, esperado|
      expect(Coach.tipo_status(nome)).to eq(esperado)
    end
  end

  it "cria um vetor com a string de versão 'v0.0.6+4'" do
    ver = Coach::Polymorph.version_as_string_list "v0.0.6+4"

    expect(ver.size).to eq 3
    expect(ver[0]).to eq 0
    expect(ver[1]).to eq 0
    expect(ver[2]).to eq 6
  end

  it "cria um vetor com a string de versão 'v1.3'" do
    ver = Coach::Polymorph.version_as_string_list "v1.3"

    expect(ver.size).to eq 2
    expect(ver[0]).to eq 1
    expect(ver[1]).to eq 3
  end
end
